# GangaWorkflow

This project illustrates how a Docker image can be built as part of the Continuous Integration and then subsequently used for executing jobs inside on the Grid via Ganga. In the example below, the image build by the
CI of this project will be used for a Ganga job.

```
j =Job()

j.virtualization=Singularity()
j.backend = Dirac()

j.virtualization.image='docker://gitlab-registry.cern.ch/egede/gangaworkflow'

j.submit()
```

