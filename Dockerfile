FROM cern/cc7-base

MAINTAINER ulrik.egede@monash.edu

COPY helloworld.sh /

RUN chmod a+rx /helloworld.sh
